package com.game.server.server

import java.nio.ByteBuffer
import java.util.*

val random = Random()

abstract class Packet(val id: Int) {
    constructor() : this(random.nextInt())

    abstract fun serialize(): ByteBuffer
    abstract fun deserialize(bytes: ByteBuffer)
}


class P00Handshake() : Packet() {
    var playerId: Int = 0

    override fun deserialize(bytes: ByteBuffer) {
        playerId = bytes.int
    }

    override fun serialize(): ByteBuffer {
        return ByteBuffer.allocate(4).putInt(playerId)
    }

}

class P01KeepAlive() : Packet() {
    var time: Long = System.currentTimeMillis()
    override fun serialize(): ByteBuffer {
        return ByteBuffer.allocate(8).putLong(time)
    }

    override fun deserialize(bytes: ByteBuffer) {
        time = bytes.long
    }

}