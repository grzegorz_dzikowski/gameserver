package com.game.server.server

import io.ktor.network.selector.ActorSelectorManager
import io.ktor.network.sockets.Socket
import io.ktor.network.sockets.aSocket
import io.ktor.network.sockets.openReadChannel
import io.ktor.network.sockets.openWriteChannel
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.net.InetSocketAddress
import java.util.concurrent.Executors

class Client(val socket: Socket) {

    val read = socket.openReadChannel()
    val write = socket.openWriteChannel()

    init {

    }

    fun handshake() {

    }

    suspend fun connect() {

    }
}

class Player(val client: Client) {

}

val players = mutableListOf<Player>()

class Server(val port: Int = 22313) {

    fun startServer() {
        runBlocking {
            val exec = Executors.newCachedThreadPool()
            val socket = aSocket(ActorSelectorManager(exec.asCoroutineDispatcher())).tcp().bind(InetSocketAddress(port))
            while (true) {

                val s = socket.accept()
                launch {
                    val client = Client(s)

                }
            }
        }
    }
}