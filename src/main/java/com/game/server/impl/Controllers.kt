package com.game.server.impl

import com.game.server.interfaces.LocalUser
import com.game.server.interfaces.LoginData
import com.game.server.interfaces.RegisterData
import com.game.server.interfaces.User
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.regex.Pattern.compile


private val emailRegex = compile(
    "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+"
)

enum class ErrorType {
    UNKNOWN, CRITICAL, INVALID_DATA, OK
}

data class ErrorCallback(var errorType: ErrorType, var errorCode: String) {
    fun s(errorType: ErrorType, error: String) {
        this.errorType = errorType
        this.errorCode = error
    }
}


interface UserController {
    fun registerUser(data: RegisterData, error: ErrorCallback): Boolean
    fun loginUser(login: LoginData, error: ErrorCallback): UUID?

    fun checkToken(token: UUID, error: ErrorCallback): Boolean
    fun logOut(token: UUID, error: ErrorCallback): Boolean


}

class SessionControllerImpl : UserController {

    val tokens = mutableMapOf<UUID, LocalUser>()

    override fun registerUser(data: RegisterData, error: ErrorCallback): Boolean {
        if (Instant.ofEpochMilli(data.birthDate).isAfter(Instant.now().minus(13, ChronoUnit.YEARS))
            || Instant.ofEpochMilli(data.birthDate).isBefore(
                Instant.now().minus(140, ChronoUnit.YEARS)
            )
        ) {
            error.errorType = ErrorType.INVALID_DATA
            error.errorCode = "invalid_date"
            return false;
        } else if (data.username.length > 30) {
            error.errorType = ErrorType.INVALID_DATA
            error.errorCode = "invalid_username"
            return false
        } else if (!emailRegex.matcher(data.email).matches()) {
            error.errorType = ErrorType.INVALID_DATA
            error.errorCode = "invalid_mail"
            return false
        } else if (data.countryCode.length != 2) {
            error.s(ErrorType.INVALID_DATA, "invalid_country")
            return false
        } else {
            transaction {
                User.new {
                    data.toUser(this)
                }
            }
            return true
        }
    }

    override fun loginUser(login: LoginData, error: ErrorCallback): UUID? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun checkToken(token: UUID, error: ErrorCallback): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logOut(token: UUID, error: ErrorCallback): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}

class AuthController {
    fun test() {

    }

}