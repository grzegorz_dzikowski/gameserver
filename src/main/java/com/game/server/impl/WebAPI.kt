package com.game.server.impl

import com.game.server.interfaces.*
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.*
import io.ktor.gson.gson
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.util.getOrFail
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import org.koin.ktor.ext.inject
import org.mindrot.jbcrypt.BCrypt
import java.util.*

fun Application.main() {
    //Default loggers, headers processor and content response manager
    install(DefaultHeaders)
    install(CallLogging)
    install(ContentNegotiation)
    {
        gson {
            setPrettyPrinting()
            serializeNulls()
        }
    }

    Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver")
    SchemaUtils.create(Users)
//    install(SwaggerSupport){
//        forwardRoot = true
//        val information = Information(
//            version = "0.1",
//            title = "sample api implemented in ktor",
//            description = "This is a sample which combines [ktor](https://github.com/Kotlin/ktor) with [swaggerUi](https://swagger.io/). You find the sources on [github](https://github.com/nielsfalk/ktor-swagger)",
//            contact = Contact(
//                name = "Niels Falk",
//                url = "https://nielsfalk.de"
//            )
//        )
//    }
    val modules = module {
        single<UserRepository> { UserRepositoryImpl() }
    }

    install(Koin) {

        modules(modules)
    }

//    install(Authentication)
//    {
//        form("client_auth") {
//            userParamName = "user"
//            passwordParamName = "pass"
//            skipWhen { applicationCall -> applicationCall.sessions.get<SessionControl>() != null }
//            validate {
//                return UserIdPrincipal("test_user")
//            }
//        }
//    }

    install(StatusPages) {
        exception<Throwable> { e ->
            e.printStackTrace() //TODO: Change this to some logger
            call.respondText(e.localizedMessage, ContentType.Text.Plain, HttpStatusCode.InternalServerError)
        }
    }

//    install(Sessions)
//    {
//        cookie<SessionControl>("MAIN_SESSION", storage = SessionStorageMemory()) //TODO: THIS IS DEV MODE ONLY
//
//    }

    val userController: UserController by inject()

    routing {

        post("/register")
        {

            val c = call.receive<RegisterData>()
            val error = ErrorCallback(ErrorType.OK, "")
            if (userController.registerUser(c, error)) {
                call.respond(Response("register_successful"))
            } else {
                call.respond(HttpStatusCode.BadRequest, Response("register_failed", error))
            }
        }


        var uuid = UUID.randomUUID()
        post("/login")
        {
            println("Login call from " + call.request.origin.remoteHost)
            val c = call.receive<LoginData>()
            if (c.username == "test" && unhashPassword(c.password, hashPassword("test"))) {
                uuid = UUID.randomUUID()
                call.respond(HttpStatusCode.OK, uuid)
            } else {
                call.respond(HttpStatusCode.Unauthorized)
            }
        }
        get("/hand")
        {
            val c = call.request.queryParameters
            if (UUID.fromString(c.getOrFail("token")) == uuid) {
                call.respond(HttpStatusCode.OK)
            } else {
                call.respond(HttpStatusCode.Unauthorized)
            }
        }
        get("/token")
        {
            call.respond("TODO: Public key to respond with...")
        }
    }
}


fun unhashPassword(pass: String, hash: String): Boolean {
    return BCrypt.checkpw(pass, hash)
}

fun hashPassword(pass: String): String {
    return BCrypt.hashpw(pass, BCrypt.gensalt())
}
