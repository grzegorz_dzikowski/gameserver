package com.game.server.interfaces

import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.upperCase
import java.util.*

interface UserRepository {

    fun findByUsername(username: String): User?

    fun existsByUsername(username: String): Boolean

    fun insertUser(user: User): Boolean
}

val random = Random()

class UserRepositoryImpl : UserRepository {
    override fun insertUser(user: User): Boolean {
        transaction {
            try {
                User.new {
                    userId = user.userId
                    email = user.email
                    countryCode = user.countryCode
                    phoneCode = user.phoneCode
                    phone = user.phone
                    username = user.username
                    displayName = user.displayName
                    tag = user.tag
                    hashPassword = user.hashPassword
                    dateOfBirth = user.dateOfBirth
                    registerTime = user.registerTime
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                return@transaction false
            }
        }
        return true
    }

    override fun existsByUsername(username: String): Boolean {
        return findByUsername(username) != null
    }

    override fun findByUsername(username: String): User? {
        transaction {
            return@transaction User.find { Users.username.upperCase() eq username.toUpperCase() }.firstOrNull()
        }
        return null
    }


}