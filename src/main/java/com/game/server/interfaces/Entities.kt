package com.game.server.interfaces

import com.game.server.impl.ErrorCallback
import com.game.server.impl.ErrorType
import com.game.server.impl.hashPassword
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import java.util.*


val emptyError = ErrorCallback(ErrorType.OK, "")

val emptyObject = object : Any() {

}

data class Response(
    val message: String,
    val error: ErrorCallback = emptyError,
    val time: Long = System.currentTimeMillis(),
    val response: Any = emptyObject
)


data class SessionControl(val name: String, val id: Int, val token: UUID)
data class LoginData(val username: String, val password: String)
data class RegisterData(
    val username: String,
    val password: String,
    val email: String,
    val birthDate: Long,
    val countryCode: String,
    val phoneCode: Int,
    val phone: Int
) {
    fun toUser(user: User) {
        user.userId = random.nextInt()
        user.countryCode = countryCode
        user.dateOfBirth = birthDate
        user.displayName = username
        user.hashPassword = hashPassword(password)
        user.phone = phone
        user.phoneCode = phoneCode
        user.registerTime = System.currentTimeMillis()
        user.username = username
        user.tag = random.nextInt(1000)
        user.email = email
    }
}

data class NormalResponse(val timestamp: String, val endpointId: Int, val message: String = "ok", val response: Any)


class User(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<User>(Users)

    var userId by Users.userId
    var email by Users.email
    var countryCode by Users.countryCode
    var phoneCode by Users.phoneCode
    var phone by Users.phone
    var username by Users.username
    var displayName by Users.displayName
    var tag by Users.tag
    var hashPassword by Users.hashPassword
    var dateOfBirth by Users.dateOfBirth
    var registerTime by Users.registerTime

    fun generateLocalUser(): LocalUser {
        return LocalUser(userId, displayName, email, tag, Locale.forLanguageTag(countryCode))
    }

}

object Users : IntIdTable() {
    val userId = integer("user_id").uniqueIndex()
    val email = text("email")
    val countryCode = varchar("country_code", 2)
    val phoneCode = integer("phone_code")
    val phone = integer("phone")
    val username = varchar("username", 30)
    val displayName = varchar("displayName", 30)
    val tag = integer("tag")
    val hashPassword = text("hash_pass")
    val dateOfBirth = long("date_of_birth")
    val registerTime = long("register_time")


}

data class LocalUser(
    val userId: Int,
    val displayName: String,
    val email: String,
    val tag: Int,
    val countryCode: Locale,
    var loginTime: Long = System.currentTimeMillis()
) {

}
